# Azure Hosting Setup #

There are two parts to this, the static files and the CORS proxy. You
can have one, the other, or both deployed on Azure. This document
assumes that you already have a Microsoft Azure account and are familiar
with the basics of logging in to and navigating around the Azure web
interface.

## Preparation ##

Before you begin the hosting setup, there are three elements that you
will need to have ready. This document will walk you through setting up
these three elements.

1. A resource group.

2. A storage account.

3. The DNS name for the CORS proxy functionapp.

### Resource Group ###

You should create a separate resource group to group together all of the
elements of this deployment.

1. In the Azure portal, hover over the Resource Groups icon and click on
   the "Create" link.

    ![azure_create_resource_group_1](images/azure_create_resource_group_1.png
    "Create Resource Group 1")

2. Give the new resource group an appropriate name, and choose the
   desired geographic location.

    ![azure_create_resource_group_2](images/azure_create_resource_group_2.png
    "Create Resource Group 2")

3. Click on "Review + Create" button, and then click on the "Create"
   button.

### Storage Account ###

You will need a storage account to hold the static files and CORS proxy
functionapp.

1. In the Azure portal, hover over the Storage Accounts icon and click
   on the "Create" link.

    ![azure_create_storage_account_1](images/azure_create_storage_account_1.png
    "Create Storage Account 1")

2. In the "Create storage account - Basics" screen, select the Resource
   Group that you just created, then:

    Enter a storage account name (3-24 characters, lowercase and numbers
    only) - The Location should be automatically set when you choose the
    Resource Group, - Set the Performance to "Standard" - Set the Account
    Kind as "StorageV2 (general purpose V2)" - Set Replication to "ZRS (Zone
    Redundant Storage)"

    ![azure_create_storage_account_2](images/azure_create_storage_account_2.png
    "Create Storage Account 2")

    Then click on the "Next: Networking" button. (Azure has many storage
    redundancy types. Since it's unlikely that you will need
    global-scale redundancy, Zone Redundant Storage is sufficient for
    this purpose.)

3. In the "Networking" screen, leave the defaults of "Public endpoint
   (all networks)" and "Microsoft network routing (default)", then click on
   the "Next: Data protection" button.

4. In the "Data protection" screen, leave all of the options unchecked,
   and click on the "Next: Advanced" button.

5. In the "Advanced" screen, leave all of the settings at their
   defaults, except for "Blob access tier (default)". Change the setting to
   "Cool", then click on the "Next: Tags" button.

    ![azure_create_storage_account_5](images/azure_create_storage_account_5.png
    "Create Storage Account 5")

6. In the "Tags" screen, set any tags that you may find useful. You can
   also just leave the storage account untagged. Click on the "Next: Review
   \+ create" button.

7. In the "Review and create" screen, check to see that all of the
   selections are correct, then click on the "Create" button.

After a minute or so, the storage account should be created and ready
for use.

### DNS Name for CORS Proxy Functionapp ###

This is not so much something you need to configure, but rather
something that you need to define before you get into the process. If
you are planning to use Azure for the CORS proxy, you should select a
unique DNS-compatible name that will be used to access the functionapp
that will serve the CORS proxy. If you are not planning to use Azure for
the CORS proxy then you can skip this part.

The full FQDN will be of the form "`<functionapp
name>.azurewebsites.net`". One way to help ensure that your name is
unique is by prefixing the functionapp name with the primary part of
your organization's domain name – e.g., if your domain name is
"`example.com`", you might choose a name of “`example-cors-proxy`”,
which results in a functionapp FQDN of
"`example-cors-proxy.azurewebsites.net`".

## Static Files Hosting ##

1. In your Azure portal dashboard, click on the link to open the storage
   account that you just created.

    ![azure_static_files_1](images/azure_static_files_1.png "Azure Static Files 1")

2. Next, click on "Storage Explorer (preview)" in the left sidebar. In
   the new column that appears, right click on the "`BLOB CONTAINERS`" line
   and select "`Create blob container`".

    ![azure_static_files_2](images/azure_static_files_2.png "Azure Static Files 2")

3. Enter "`$web`" as the name of the new container, and set the access
   level to "`Blob (anonymous read access for blobs only)`", then click on
   the "Create" button.

    ![azure_static_files_3](images/azure_static_files_3.png "Azure Static Files 3")

4. Once the `$web` container has been created, click on it to select it,
   then click on the "Upload" button.

    ![azure_static_files_4](images/azure_static_files_4.png "Azure Static Files 4")

5. Download the four files to your computer (right click on each link
   and select "Download Linked File" or "Save Link As..."):

    <https://legalserver-tableau-connector.bitbucket.io/connector_code/legalserver-tableau-connector.html>  
    <https://legalserver-tableau-connector.bitbucket.io/connector_code/legalserver-tableau-connector.css>  
    <https://legalserver-tableau-connector.bitbucket.io/connector_code/legalserver-tableau-connector.js>  
    <https://legalserver-tableau-connector.bitbucket.io/connector_code/LegalServer-Tableau.png>  

6. Click on the folder icon and select the four static files to be
   uploaded, then click on the upload button at the bottom. Once the upload
   is complete you should see that the four files have been uploaded under
   "Current uploads".

    ![azure_static_files_6](images/azure_static_files_6.png "Azure Static Files 6")

7. Click on the "Static website" link in the left sidebar under
   "Settings". Switch the slider from "Disabled" to "Enabled", then click
   on the "Save" button at the top. Once the site has been enabled, note
   the Primary endpoint for the static web site.

    ![azure_static_files_7](images/azure_static_files_7.png "Azure Static Files 7")

8. Test the static web site by loading the uploaded HTML file in a
   browser. For instance, if the Primary endpoint is:

    `https://lstableaustorage.z20.web.core.windows.net/`

    then you should try to load:

    `https://lstableaustorage.z20.web.core.windows.net/legalserver-tableau-connector.html`

    You should see a well-formed web page that looks like the picture in
    the README file, with nicely styled text and a visible logo. Try
    entering a few characters in the "Connection name" field and click
    in another field. The little warning "Required" below the label
    should go from red to black, which means that the JavaScript is
    loaded and working.

## CORS Proxy Setup ##

1. From the Azure Portal, click on the "Create a resource" icon

    ![azure_cors_proxy_1](images/azure_cors_proxy_1.png "Azure CORS Proxy 1")

2. Select “Function App” as the resource type. If it doesn’t appear in
   the column of popular resources, you may need to do a search using the
   “Search the Marketplace” field.

    ![azure_cors_proxy_2](images/azure_cors_proxy_2.png "Azure CORS Proxy 2")

3. Select the Resource Group that you created earlier, then fill in the
   FunctionApp name field using the DNS name that you chose in the
   preparation step 3. Other values should be:

    Publish: “Code” Runtime stack: “.Net Core” Version: “3.1” Region:
    Whatever you want, or leave the default (I’ve been using “East US
    2”).

    Click on the “Next: Hosting” button to continue.

    ![azure_cors_proxy_3](images/azure_cors_proxy_3.png "Azure CORS Proxy 3")

4. Select your storage account, choose Windows as the operating system,
   and set Plan type to “Consumption (Serverless)”. Click on the “Next:
   Monitoring” button to continue.

    ![azure_cors_proxy_4](images/azure_cors_proxy_4.png "Azure CORS Proxy 4")

5. Enable Application Insights and create a new Insights resource for
   the functionapp. Click on the “Next: Tags” button to continue.

    ![azure_cors_proxy_5](images/azure_cors_proxy_5.png "Azure CORS Proxy 5")

6. Add any tags that you want, then click on the “Next: Review + create”
   button.

7. Check that the values are correct and then click on the “Create”
   button to create the functionapp. It may take a few minutes.

8. Once the functionapp is created, scroll the left sidebar until you
   find the Settings section, then click on the Configuration selection. In
   the Configuration pane, click on the "New application setting" link.

    ![azure_cors_proxy_8](images/azure_cors_proxy_8.png "Azure CORS Proxy 8")

9. Add the following setting:
    `AZURE_FUNCTION_PROXY_BACKEND_URL_DECODE_SLASHES` with the value `true`,
    click on the OK button, then click on the "Save" button at the top, and
    allow the functionapp to restart. Without this setting, Azure will
    URL-encode any slashes in the URL sent to the proxy as “%2F”, which will
    not create the correct path for the back end URL.

    ![azure_cors_proxy_9](images/azure_cors_proxy_9.png "Azure CORS Proxy 9")

10. Scroll down the left sidebar until you find the API section, then
    click on the CORS selection. Add the URL for the static site that you
    just created to the list of Allowed Origins, then click on the "Save"
    button at the top.
    
    ![azure_cors_proxy_10](images/azure_cors_proxy_10.png "Azure CORS Proxy 10")

11. Click on the “Proxies” selection in the left sidebar, then click on
    the “Add” button at the top. Enter a name for the proxy. This is just
    used as a human-readable label and has no operational significance.

    The Route template is a path used by a requester to access the
    proxy. It should have the form “`/<somename>/{*rest}`”. The
    suggested name for the route template would be the DNS name of your
    LegalServer instance. E.g., if your LegalServer instance is
    "`example.legalserver.org`", you would enter
    "`/example-legalserver-org/{*rest}`".

    The Backend URL points to the target host for the proxy. It should
    have the form “`https://<target fqdn>/{rest}`”. E.g, if your
    LegalServer instance is "`example.legalserver.org`", you would enter
    "`https://example-legalserver-org/{rest}`".

    Click on the "Create" button at the bottom to complete the Proxy
    setup.

    ![azure_cors_proxy_11](images/azure_cors_proxy_11.png "Azure CORS Proxy 11")

12. Once you have created the proxy, copy and save the Proxy URL from
    the pane. You will need to enter this in the Web Data Connector window
    when you run it from within Tableau. The Proxy URL from Azure will look
    like:
    
    `https://example-legalserver-tableau.azurewebsites.net/example-legalserver-org/{*rest}`
    
    You will need to trim off the `{*rest}` segment when you paste it
    into the Web Data Connector window. The final URL that you will use
    should look like:
    
    `https://example-legalserver-tableau.azurewebsites.net/example-legalserver-org/`
    
    ![azure_cors_proxy_12](images/azure_cors_proxy_12.png "Azure CORS Proxy 12")

13. Repeat steps 11 and 12 for your LegalServer demo site, so that you
    can test the connector before using this with real data. Since the
    LegalServer demo sites have URLs that look like:

    `example-demo.legalserver.org`

    The Route template for the demo site would become:

    `/example-demo-legalserver-org/{*rest}`

    The Backend URL would be:

    `https://example-demo-legalserver-org/{rest}`

    The Proxy URL would be:

    `https://example-legalserver-tableau.azurewebsites.net/example-demo-legalserver-org/{*rest}`

    The final URL for the Web Data Connector would be:

    `https://example-legalserver-tableau.azurewebsites.net/example-demo-legalserver-org/`

    ![azure_cors_proxy_13](images/azure_cors_proxy_13.png "Azure CORS Proxy 13")


Your setup in Azure is now complete and you are ready to set up the Web
Data Connector.




