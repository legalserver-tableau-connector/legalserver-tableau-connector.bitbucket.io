# LegalServer-Tableau Connector #

This is a connector written in HTML and JavaScript for importing data
from a [LegalServer.org](https://www.legalserver.org/) instance to
Tableau, using a [Web Data
Connector](https://tableau.github.io/webdataconnector/docs/). It was
originally written as a *pro bono* project for the [Children's Law
Center](https://www.childrenslawcenter.org/) in Washington DC.

From the Children's Law Center: 

>CLC uses Tableau to provide valuable insights on program performance
>and effectiveness to non-technical program managers and organization
>leadership. The dataconnector has been extremely successful in
>optimizing our ability to use data in real-time and reduce time and
>resources needed to manually connect reports. This has increased our
>ability to measure program performance, progress towards goals and
>deliverables, and engage with our data in new and exciting ways.

Since it may be useful for other entities that are using LegalServer and
would like to run reports using Tableau, the Children's Law Center
kindly allowed me to release this as an open source project.

### Prerequisites ###

Like some other Tableau web data connectors this connector requires both a
web server that can serve static files and a CORS proxy, which can be on
the same server or a separate server.

While Tableau suggests hosting the static files on Github and the CORS
proxy on Heroku free tier, we weren't completely comfortable with that
arrangement since were dealing with potentially sensitive data at CLC.
Instead, this connector was deployed using Microsoft Azure to serve the
static files and act as the CORS proxy. There are separate documents
that cover [how to do the setup on Azure](Azure_Setup.md) and [how to
do the setup on Amazon Web Services](AWS_Setup.md).

### Static Files Server Setup ###

I have set up a page to serve the static files from BitBucket, which is
the repository that I use. The URL for the pages is:

><https://legalserver-tableau-connector.bitbucket.io/connector_code/legalserver-tableau-connector.html>

I would strongly suggest that you host the static files yourself if you
are using this connector for production. You can use any web server;
place all four of the files in the `connector_code` subdirectory
(`legalserver-tableau-connector.html`,
`legalserver-tableau-connector.css`, `legalserver-tableau-connector.js`,
and `LegalServer-Tableau.png`) in the same directory on the web server.
Try loading the HTML file using a browser to make sure that they are
accessible and load correctly. While it is not absolutely necessary, it
is strongly recommended that the static files be served via HTTPS.

If you follow the instructions for setting up the static files hosting
on Azure or AWS in the accompanying how-to document you will be all set.

### CORS Proxy Setup ###

(If you're not certain what a CORS proxy does and why it's necessary,
[Tableau has good documentation](https://tableau.github.io/webdataconnector/docs/wdc_cors.html) .)

Note: unlike some Tableau web data connectors, I have not set up a
Heroku CORS proxy for others to use as suggested in the documentation.
Almost anyone who will be using this connector will be transferring
sensitive or even attorney-client privileged data, which you definitely
do not want to expose to a third party and potentially open up to
discovery. If you use a proxy that is controlled by another entity, that
entity will be able to read any data sent through the proxy.

You will need determine the eventual URL for your CORS proxy, in order
to configure the connector the first time it runs. Some proxies take the
target URL as a value for the query variable. If the actual data would
come from

- `https://target-host.example.org/some/path`

then the full proxy URL would be:

- `https://cors-proxy.example.com/proxy/?target=https://target-host.example.org/some/path`

Others will fix the target host in the proxy configuration and just
accept the path portion:

- `https://cors-proxy.example.com/proxy?targetpath=/some/path`

Your CORS proxy **MUST** be protected by HTTPS.

*If you are not able to set up your CORS proxy to be accessed using only
HTTPS and not HTTP, you should not use this connector*.

Again, if you follow the instructions for setting up the CORS Proxy on
Azure or AWS in the accompanying How-To documents you will be all set.

### Setting Up the XML Feed From LegalServer.org ###

Follow the [instructions from
LegalServer.org](http://help.legalserver.org/home/reports/reports-api)
to set up the feed.

You will need to file a ticket with LegalServer.org to have the Reports
API activated for your instance.

*Please be extremely careful about who is able to access your data
within Tableau. You do not want to allow unauthorized people to view
privileged or sensitive information just for some enhanced reports.*

You will need to define a report within LegalServer.org to feed the data
to Tableau. That report will be used to create the XML feed that the
connector pulls from LegalServer.org.

### Running the Connector From Tableau ###

One you have the static files, CORS proxy, and XML feed in place, you
can then access the connector from within Tableau to retrieve data from
LegalServer.org.

Instructions from Tableau's web site:

- [Tableau Desktop](https://tableau.github.io/webdataconnector/docs/wdc_use_in_tableau.html)
- [Tableau Server](https://tableau.github.io/webdataconnector/docs/wdc_use_in_server.html)

When the connector window pops up, fill in the connection information in
the form. The fields are:

![Connection info window](images/connector_base_info.png "Connection
base info")

1. Connection Name: a name that is used to identify this connection
   within Tableau; something like "LegalServer Reports Feed"

2. Username: the username that you created for Reports API access within
   LegalServer.org

3. Password: the password for that user

4. Report API URL: the URL for the report from the API page on
   LegalServer.org. It should look something like this:

    `https://example-demo.legalserver.org/modules/report/api_export.php?load=101&api_key=f6554129-3777-45c9-8966-0ac75f6ddfe2`

5. CORS Proxy Base URL: the part of the CORS proxy URL that doesn't
   change no matter which URL the proxy is retrieving. For instance, if the
   full proxy URL is

    `https://cors-proxy.example.com/proxy?targetpath=/some/path`

    then the base URL would be

    `https://cors-proxy.example.com/proxy?targetpath=`

    If you are using Azure as per the setup in the accompanying document,
    the CORS Proxy URL is in step 12 or 13 from the setup document. If
    you are using AWS as per the setup in the accompanying document, the
    CORS Proxy URL for AWS is in step 8 from the setup document. 

6. Use Complete Report API URL: Check this box if your CORS proxy URL
   should include the full Report API URL like this:

    `https://cors-proxy.example.com/proxy/?target=https://target-host.
    example.org/some/path`

    Do not check the box if your CORS proxy URL only contains the path
    portion:

    `https://cors-proxy.example.com/proxy?targetpath=/some/path`

    If you are using Azure or AWS as per the setup in the accompanying
    document, you should *not* check this box.

7. Derived Actual URL: This is a calculated field that shows the final
   URL that Tableau will use to retrieve the report data. It is a
   combination of the Report API URL, CORS Proxy Base URL, and the Use
   Complete Report API URL check box. Use this to check if your URL entries
   and check box setting are correct.

8. Report ID: An ID for the report, which should contain no spaces

9. Report Alias: A human-readable name for the report

Next, fill in the report schema details. This is a slightly tedious
process, although you only have to do it once. Click on the green "Add
Field" (**A**) button to add information for an additional field in your
schema. If you have an extra line, click on the corresponding red
"Delete Field" (**B**) button to delete it.

![Schema details](images/schema_details.png "Schema details")

For each data field, you need to enter:

1. ID: the ID tag for the field in the XML

2. Alias: a human-readable version of the ID

3. Data Type: There are seven data types. Five of them just pass the
data through to Tableau (bool, float, geometry, int, string). Date and
datetime will re-format the data so that Tableau can read the date and
time properly.

*Note: Only the string and datetime data types have been tested. While I
believe the others will work, they have not been tested and may not work
properly. Use them at your own risk.*

When you have entered all of the connection information, click on the
"Get LegalServer Data" button at the bottom. Your data should be pulled
into Tableau after a minute or two. If it gives an error or hangs, see
the Tableau documentation on how to [troubleshoot the connector](https://tableau.github.io/webdataconnector/docs/wdc_debugging.html#debug-chrome).

### License ###

This software is released under the [GNU Affero General Public License,
version 3](https://www.gnu.org/licenses/agpl-3.0.en.html). See the file
LICENSE.txt in the repository or go to the web site for the full text.

### Contact ###

Paul Suh

<mailto:legalserver-tableau-connector@mspex.net>

