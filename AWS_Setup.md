# AWS Hosting Setup #

There are two parts to this, the static files and the CORS proxy. You
can have one, the other, or both deployed on AWS. This document assumes
that you already have a AWS account and are familiar with the basics of
logging in to and navigating around the AWS web interface.

## Preparation ##

This is not so much something you need to configure, but rather
something that you need to define before you get into the process. If
you are planning to use AWS, you should select a unique DNS-compatible
name that will be used to access the S3 bucket for the static files.

The complete FQDN will be of the form "`<DNS name>.s3.amazonaws.com`",
and must be unique across all AWS buckets. One way to help ensure that
your name is unique is by prefixing the functionapp name with the
primary part of your organization's domain name – e.g., if your domain
name is "`example.com`", you might choose a name of
“`example-ls-tableau-connector`”, which results in a FQDN of
"`example-ls-tableau-connector.s3.amazonaws.com`".

## Static Files Hosting ##

1. In the AWS console, go to the S3 dashboard under the Storage section.
   Create an S3 bucket to hold the static files. Go to the AWS S3
   management web page and click on the "Create Bucket" button.

    ![aws_static_files_1](images/aws_static_files_1.png "AWS Static
    Files 1")

2. Give the new bucket the DNS name that you determined in the
   preparation stage. Select a region that is appropriate for your
   usage.

    ![aws_static_files_2](images/aws_static_files_2.png "AWS Static
    Files 2")

3. Scroll down and uncheck the box, "Block *all* public access", then
   check the box, "I acknowledge that the current settings might result
   in this bucket and the objects within becoming public." Scroll to the
   bottom and click on the "Create Bucket" button.

    ![aws_static_files_3](images/aws_static_files_3.png "AWS Static
    Files 3")

4. Once the bucket has been created, click on the link in the console to
   go into the bucket.

    ![aws_static_files_4](images/aws_static_files_4.png "AWS Static
    Files 4")

5. Download the four files to your computer (right click on each link
   and select "Download Linked File" or "Save Link As..."):

    <https://legalserver-tableau-connector.bitbucket.io/connector_code/legalserver-tableau-connector.html>  
    <https://legalserver-tableau-connector.bitbucket.io/connector_code/legalserver-tableau-connector.css>  
    <https://legalserver-tableau-connector.bitbucket.io/connector_code/legalserver-tableau-connector.js>  
    <https://legalserver-tableau-connector.bitbucket.io/connector_code/LegalServer-Tableau.png>  

6. Click on the orange Upload button. On the following page, select the
   four files that you just downloaded by dragging and dropping, or
   clicking the "Add Files" button. Click on the "Upload" at the bottom
   to execute the upload, then click on the "Close" button to go back to
   the top level of the bucket management.

    ![aws_static_files_6](images/aws_static_files_6.png "AWS Static
    Files 6")

7. You should see all four files that you uploaded in the bucket. Check
   all of the boxes next to the four files, click on the "Actions"
   button, and select "Make public". Click on the "Make public" button 
   at the bottom of the confirmation screen that follows.

    ![aws_static_files_7](images/aws_static_files_7.png "AWS Static
    Files 7")

8. After you make the files public, you get a confusingly-designed
    status page. At the top you will get a notice, "Successfully
    edited public access. View details below." In the middle the
    Summary will state, "Successfully edited public access 4 objects,
    30.2 KB". However, at the bottom the section header will state,
    "Failed to edit public access (0)". The section header is just
    indicating that there were no files where there were problems.
    Close the status page by clicking on the "Close" button at the
    upper right-hand part of the page.

    ![aws_static_files_8](images/aws_static_files_8.png "AWS Static
    Files 8")

9. Back at the main level of the bucket, click on the link for the
    HTML file. On the information page of the file, copy and save the
    Object URL. You will need this both for step 13 in the CORS Proxy
    Setup below as well as the Web Data Connector itself. 

    ![aws_static_files_9](images/aws_static_files_9.png "AWS Static
    Files 9")

10. Open the URL in a private or incognito browser. It should look like
    the images in the main README document. This will check to see that
    the logo, CSS, and JavaScript are all being loaded properly, as well
    as checking that you don't need to be logged in to access the files.


## CORS Proxy Setup ##

You will need to go through these steps twice, once for your actual
instance and once for your demo instance. In step 3 you will need to
use a different name, and in steps 5 and 6 you will need to put in the
URL for your demo LegalServer.org instance rather than your live
instance. 

1. In the AWS console, go to the API Gateway under the Networking &
   Content Delivery section. Click on the "Create API" button in the
   upper right-hand corner.

    ![aws_cors_proxy_1](images/aws_cors_proxy_1.png "AWS CORS Proxy 1")

2. Click on the "Build" button under the HTTP API type.

    ![aws_cors_proxy_2](images/aws_cors_proxy_2.png "AWS CORS Proxy 2")

3. Enter a name for the API Gateway (such as "LegalServer CORS
    Proxy"), then click on the "Add integration" button. 

    ![aws_cors_proxy_3](images/aws_cors_proxy_3.png "AWS CORS Proxy 3")

4. Click on the pop-up menu and select "HTTP". 

    ![aws_cors_proxy_4](images/aws_cors_proxy_4.png "AWS CORS Proxy 4")

5. Select "OPTIONS" from the pop-up menu, and enter the URL to your
    LegalServer.org instance (e.g., `https://example.legalserver.org`). 

    ![aws_cors_proxy_5](images/aws_cors_proxy_5.png "AWS CORS Proxy 5")

6. Click on the "Add integration" button again, and repeat steps 4
    and 5, selecting "HTTP", "ANY", and entering the URL for your
    LegalServer.org instance again. Click on the "Next" button to
    continue. 

    ![aws_cors_proxy_6](images/aws_cors_proxy_6.png "AWS CORS Proxy 6")

7. On the Routes screen, change the first route to have the Method
    "OPTIONS" and the Resource path `/{proxy+}`. The Integration
    target for the first route should be the OPTIONS integration.
    Change the second route to have the Resource path `$default` (the
    Method will automatically be changed to "`-`"). The Integration
    target for the second route should be the ANY integration. 

    ![aws_cors_proxy_7](images/aws_cors_proxy_7.png "AWS CORS Proxy 7")

8. Leave the "$default" stage in place and click on the "Next" button
    to continue. 

9. Review the settings and click on the "Create" button to create the
    CORS proxy. 

    ![aws_cors_proxy_9](images/aws_cors_proxy_9.png "AWS CORS Proxy 9")

10. Your proxy will be created, and you will be returned to a screen
    that looks like the following. Copy and save the Invoke URL for
    the proxy. You will need to enter it in the Web Data Connector as
    the CORS PRoxy Base URL. 

    ![aws_cors_proxy_10](images/aws_cors_proxy_10.png "AWS CORS Proxy 10")

11. Click on the "CORS" link in the left sidebar. 

    ![aws_cors_proxy_11](images/aws_cors_proxy_11.png "AWS CORS Proxy 11")

12. Click on the "Configure" button at the top right. 

    ![aws_cors_proxy_12](images/aws_cors_proxy_12.png "AWS CORS Proxy 12")

13. Enter just the host part of the URL that you saved from step 9 of
    the Static Files Hosting setup into the
    "Access-Control-Allow-Origin" field, then click on the "Add"
    button next to it. For instance, if the full URL from step 9 is
    `https://example-ls-tableau-connector.s3.amazonaws.com/legalserver-tableau-connector.html`, then you would enter just
    `https://example-ls-tableau-connector.s3.amazonaws.com/`. 

    ![aws_cors_proxy_13](images/aws_cors_proxy_13.png "AWS CORS Proxy 13")

14. From the "Access-Control-Allow-Methods" pop-up menu, select the
    "OPTIONS and "GET" methods. 

    ![aws_cors_proxy_14](images/aws_cors_proxy_14.png "AWS CORS Proxy 14")

15. Enter "*" into the Access-Control-Allow-Headers field, then click
    on the "Add" button next to it. 

    ![aws_cors_proxy_15](images/aws_cors_proxy_15.png "AWS CORS Proxy 15")

16. Your final CORS configuration should look similar to the
    following, only with your URL in the Access-Control-Allow-Origin
    section. Click the "Save" button to continue. 

    ![aws_cors_proxy_16](images/aws_cors_proxy_16.png "AWS CORS Proxy 16")

Repeat steps 1-16 for your LegalServer demo site. 

- In step 3, use a name that makes it clear that this proxy is for
    the demo site, like "LegalServer Demo CORS Proxy". 

- In steps 5 and 6, use the URL for your LegalServer demo instance
    rather than the URL for your live data (e.g.,
    `https://example-demo.legalserver.org`).

- In step 13, use the *same* URL that you used for your actual instance. 

- Note and save the different Invoke URL for your demo proxy. 

Your setup in AWS is now complete and you are ready to set up the Web
Data Connector.







