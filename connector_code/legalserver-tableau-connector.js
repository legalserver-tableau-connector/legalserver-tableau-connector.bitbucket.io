/*
    Copyright ©2020,2021 Paul Suh. 
    
	This file is part of the LegalServer-Tableau Connector.

    The LegalServer-Tableau Connector is free software: you can
    redistribute it and/or modify it under the terms of the GNU Affero
    General Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later
    version.

    The LegalServer-Tableau Connector is distributed in the hope that it
    will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU General Public License for more details.

    You should have received a copy of the GNU Affero General Public
    License version 3 along with the LegalServer-Tableau Connector.  If 
    not, see <https://www.gnu.org/licenses/>.
*/
$(document).ready(function(){

	// load plug-in for day.js
	dayjs.extend(window.dayjs_plugin_customParseFormat);

	
	// create event listener for adding rows
	$("#addFieldButton").on( "click", function() {

		var tableDiv = $("#schemaTable");
		
		// Create cell for ID
		var newIdDiv = $("<div></div>");
		var newIdInput = $("<input>")
			.prop("type","text")
			.addClass("form-control")
			.addClass("schema-input")
			.addClass("schema-id");
		$(newIdDiv).append(newIdInput);
		$(tableDiv).append(newIdDiv);
		
		// Create cell for Alias
		var newAliasDiv = $("<div></div>");
		var newAliasInput = $("<input>")
			.prop("type","text")
			.addClass("form-control")
			.addClass("schema-input")
			.addClass("schema-alias");
		$(newAliasDiv).append(newAliasInput);
		$(tableDiv).append(newAliasDiv);
		
		// Create cell for Data Type
		var newDatatypeDiv = $("<div></div>");
		var newSelect = $("<select></select>")
			.prop( "name", "datatype" )
			.addClass("schema-datatype");
		var optionsList = [ 
			"bool", 
			"date", 
			"datetime", 
			"float", 
			"geometry", 
			"int", 
			"string" ];
		optionsList.forEach( function( currentValue ) {
			var newOption = $("<option></option>").text(currentValue);
			if ( currentValue == "string" ) {
				newOption.prop("selected", true);
			}
			$(newSelect).append( newOption );
		});
		$(newDatatypeDiv).append(newSelect);
		$(tableDiv).append(newDatatypeDiv);
		
		// Create cell for Delete Field button
		var newDeleteButtonDiv = $("<div></div>");
		var newButton = $("<button></button>")
			.addClass("small-button")
			.addClass("red-button")
			.text("Delete Field")
			.on( "click", function( buttonElement ) {
				var parentDiv = $(buttonElement.target).parent();
				
				// go up one sibling node then delete three times
				for ( var i = 0; i < 3; i++ ) {
					$(parentDiv).prev().remove();
				}
				// then delete current node
				$(parentDiv).remove();
			});
		$(newDeleteButtonDiv).append(newButton);
		$(tableDiv).append(newDeleteButtonDiv);

	});
	
	
	// Check the field to see if it's empty. IF it is, set the "(Required)" 
	// tag to red by adding the class required-field-tag-red. If it's not, 
	// remove the class. 
	function checkForEmptyField() {
	
		var requiredTagFieldId = "#" + $(this).attr("id") + "RequiredTag";
		if ( $(this).val() === '' ) {
			$(requiredTagFieldId).addClass( "required-field-tag-red" );
		} else {
			$(requiredTagFieldId).removeClass( "required-field-tag-red" );
		}
		
		// check to see if any required fields are empty. If so, set disabled
		// on the submit button. If not, remove disabled. 
		if ( $(".required-field-tag-red").length == 0 ) {
			$("#submitButton").prop( "disabled", false );
		} else {
			$("#submitButton").prop( "disabled", true );
		}
		
	}
	
	// Bind the checkForEmptyField() function to any input or textarea
	// that has a tag in the HTML
	$( ".required-field-tag" ).each( function( index, requiredTag ) {
		
		var inputFieldId = $(requiredTag).attr("id").replace( "RequiredTag", "" );
		$("#" + inputFieldId).on( "change", checkForEmptyField )
		
	});
	
	// Calculate the actual URL live (whether or not using proxy, and 
	// whether or not using full URL or partial URL)
	function buildActualUrl() {
		
		// if no proxy, then actualUrl is same as reportApiUrl
		if ( $("#corsProxyBaseUrl").val().length == 0 ) {
			$("#actualUrl").val( $("#reportApiUrl").val() );
		} else if ( $("#useCompleteReportApiUrlCheckbox").is(":checked") ) {
			$("#actualUrl").val( $("#corsProxyBaseUrl").val() + $("#reportApiUrl").val() );
		} else {
			var reportApiUrlObject = new URL( $("#reportApiUrl").val() );
			$("#actualUrl").val( $("#corsProxyBaseUrl").val() + reportApiUrlObject.pathname 
				+ reportApiUrlObject.search );
		}
	}
	
	// Bind to keyup event on the two URL fields and to change event for
	// the checkbox. Using the keyup event means that the URL changes live
	// with typing, and also avoids conflict with the change event used
	// for checking if the field is empty. 
	$("#reportApiUrl").on( "keyup", buildActualUrl );
	$("#corsProxyBaseUrl").on( "keyup", buildActualUrl );
	$("#useCompleteReportApiUrlCheckbox").on( "change", buildActualUrl );


	// Traverses the schema table and builds an array
	function buildColumnsArray() {
	
		var colsArray = $(".schema-id").map( function() {
			
			// get parent div
			var schemaIdDiv = $(this).parent();
			
			// get other divs
			var schemaAliasDiv = $(schemaIdDiv).next();
			var schemaDatatypeDiv = $(schemaAliasDiv).next();
			
			return {
				id: $(schemaIdDiv).find(".schema-id").val(),
				alias: $(schemaAliasDiv).find(".schema-alias").val(),
				
				// this switch statement is just paranoid programming
				// tableau.dataTypeEnum.<whatever> is defined as a string
				// "whatever", but it's supposed to be opaque so we'll go
				// with it
				dataType: ( function( dtString ) { 
					var dt = "no match";
					switch ( dtString ) {
						case "bool": 
							dt = tableau.dataTypeEnum.bool;
							break;
						case "date":
							dt = tableau.dataTypeEnum.date;
							break;
						case "datetime":
							dt = tableau.dataTypeEnum.datetime;
							break;
						case "float":
							dt = tableau.dataTypeEnum.float;
							break;
						case "geometry":
							dt = tableau.dataTypeEnum.geometry;
							break;
						case "int":
							dt = tableau.dataTypeEnum.int;
							break;
						case "string":
							dt = tableau.dataTypeEnum.string;
							break;
					};
					return dt;
				})( $(schemaDatatypeDiv).find(".schema-datatype").val() ),
			};
		});
		
		// this needs to be a JS array, not a HTMLCollection object
		return Array.from( colsArray );
	};
	

	// Create event listener for when the user submits the form
	$("#submitButton").on( "click", function() {

		// Set the data source name in Tableau
		tableau.connectionName = $("#connectionName").val();
		
		// Username and password
		tableau.password = $("#password").val();
		tableau.username = $("#username").val();
		
		// Build connection data object. 
		var connData = {
			actualUrl: $("#actualUrl").val(),
			schemaInfo: {
					id: $("#reportId").val(),
					alias: $("#reportAlias").val(),
					// code to build the columns array is complex enough that 
					// it's broken out into another function
					columns: buildColumnsArray(),
				},
		};
		
		// Need to serialize connection data object, since Tableau wants it
		// to be a string. Fortunately, JSON should work
		tableau.connectionData = JSON.stringify( connData );
		
		tableau.submit(); // This sends the connector object to Tableau
	});


	// Create the connector object
	var myConnector = tableau.makeConnector();
	

	myConnector.getSchema = function(schemaCallback) {
	
		// Pull schema from the connection data which is JSON
		// There is a property called "schemaInfo" that holds an object with
		// the schema information. 
		
		// Turn connection data JSON string back into an object
		var connDataObj = JSON.parse( tableau.connectionData );
		var tableSchema = connDataObj.schemaInfo;
	
		schemaCallback([tableSchema]);
	};


	// Download the data
	// Create an anonymous function that will be executed by the connector after
	// the first phase where the schema is defined. 
	myConnector.getData = function(table, doneCallback) {
		
		// Turn connection data JSON string back into an object
		var connDataObj = JSON.parse( tableau.connectionData );
		
		// execute a JQuery AJAX call to retrieve the data
		// Authorization: header is added manually to avoid a second round-trip
		// which would happen if you used the JQuery ajax() object's built-in
		// username and password properties.  
		$.ajax({
			"url": connDataObj.actualUrl,
			headers: {
				"Authorization": "Basic " + btoa( tableau.username + ":" + tableau.password ),
			},
			"datatype": "xml",
		
			// Anonymous function executed on successful retrieval of XML
			// This part is responsible for parsing the XML and putting it
			// into a JavaScript array that is handed off to Tableau. 
			// Since the response is XML we need to use DOM-style parsing to dig out 
			// individual elements. 
			"success": function(resp) {
				if (resp == false) {

					tableau.log('Authentication Or Request Error, Legal Server Report API Returned ' + resp);
					tableau.abortWithError('Authentication Or Request Error, Legal Server Report API Returned ' + resp);
					return;
				}
				
				// iterate over each row
				var tableTemp = $(resp).find("row").map( function( currentIndex, currentRow ) {
					
					// for each row, iterate over the list of column id's and pull out the
					// column data for each id
					return connDataObj.schemaInfo.columns.reduce( function( currRowObj, currentColumn ) {
						
						// For one column id, find all of the matching XML tags, then
						// put the content into the current row object. If there are 
						// multiple matching XML tags, then create a semicolon-separated
						// list as the value. 
						currRowObj[currentColumn.id] = $(currentRow).find(currentColumn.id)
							// This complicated expression handles multiple occurrences
							// of the same tag, putting them into a semicolon-delimited list
							.map( function() {
								dataValue = $(this).text();
								
								// Re-format the data if it is a date-time so that 
								// Tableau can parse it. Tableau is apparently pretty picky
								// about the format of incoming datetime values
								if ( currentColumn.dataType == tableau.dataTypeEnum.datetime ) {
									dataValue = dayjs( dataValue, "YYYY-MM-DDTHH:mm:ss-00:00" )
										.format( "YYYY-MM-DD HH:mm:ss" );
								}
								return dataValue;
							}).get().join(';');
						
						return currRowObj;
						
					}, {} );
				});
				
				// Convert from JQuery HTMLCollection to JS Array
//				table.appendRows( Array.from(tableTemp) );
                appendDataChunked( table, Array.from(tableTemp) );
				
				// execute the callback function
				doneCallback();
			},
		});
	};

	tableau.registerConnector(myConnector);
	
	function appendDataChunked( table, dataArray ) {
	
	    var row_index = 0;
	    var size = 1000;
	    
	    while ( row_index < dataArray.length ) {
	    
	        table.appendRows( dataArray.slice( row_index, size + row_index ) );
	        row_index += size;
	        tableau.reportProgress( "Getting row: " + row_index );
	        
	    }
	}


});
